#include "Alaqsa.h"
#include <windows.h>
#include <gl\gl.h>
#include <ctime>
#include <cmath>
#include "texture.h"

void Alaqsa::Draw_alaqsa(int door, int wallMosque, int carpet)
{
//glTranslated(0, 0, -20);
	
	
		//first door
		glBindTexture(GL_TEXTURE_2D, door);

		glBegin(GL_QUADS);
		glTexCoord2d(0, 0);
		glVertex3d(400, -200, -6700);

		glTexCoord2d(1, 0);
		glVertex3d(1000, -200, -6700);

		glTexCoord2d(1, 1);
		glVertex3d(1000, 250, -6700);

		glTexCoord2d(0, 1);
		glVertex3d(400, 250, -6700);

		glEnd();

	//first wall (on the left)
		glBindTexture(GL_TEXTURE_2D, wallMosque);

		glBegin(GL_QUADS);

		glTexCoord2d(0, 0);
		glVertex3d(-200, -200, -7000);

		glTexCoord2d(1, 0);
		glVertex3d(400, -200, -6700);

		glTexCoord2d(1, 1);
		glVertex3d(400, 250, -6700);

		glTexCoord2d(0, 1);
		glVertex3d(-200, 250, -7000);


		glEnd();

		
	//second door

		glBindTexture(GL_TEXTURE_2D, door);

		glBegin(GL_QUADS);

		glTexCoord2d(0, 0);
		glVertex3d(-200, -200, -7000);

		glTexCoord2d(1, 0);
		glVertex3d(-200, -200, -7300);


		glTexCoord2d(1, 1);
		glVertex3d(-200, 250, -7300);

		glTexCoord2d(0, 1);
		glVertex3d(-200, 250, -7000);

		glEnd();
		
	//second wall
		glBindTexture(GL_TEXTURE_2D, wallMosque);
		glBegin(GL_QUADS);
		glTexCoord2d(0, 0);
		glVertex3d(400, -200, -7600);

		glTexCoord2d(0, 1);
		glVertex3d(400, 250, -7600);

		glTexCoord2d(1, 1);
		glVertex3d(-200, 250, -7300);

		glTexCoord2d(1, 0);
		glVertex3d(-200, -200, -7300);
		glEnd();
	//third door
			glBindTexture(GL_TEXTURE_2D, door);

		glBegin(GL_QUADS);
		glTexCoord2d(0, 0);
		glVertex3d(400, -200, -7600);

		glTexCoord2d(1, 0);
		glVertex3d(1000, -200, -7600);

		glTexCoord2d(1, 1);
		glVertex3d(1000, 250, -7600);

		glTexCoord2d(0, 1);
		glVertex3d(400, 250, -7600);

		glEnd();
		
	//third wall 
	glBindTexture(GL_TEXTURE_2D, wallMosque);
		glBegin(GL_QUADS);
		glTexCoord2d(0, 0);
		glVertex3d(1000, -200, -7600);

		glTexCoord2d(0, 1);
		glVertex3d(1000, 250, -7600);

		glTexCoord2d(1, 1);
		glVertex3d(1600, 250, -7300);

		glTexCoord2d(1, 0);
		glVertex3d(1600, -200, -7300);
		glEnd();
		
	//fourth door
		glBindTexture(GL_TEXTURE_2D, door);
		glBegin(GL_QUADS);
		glTexCoord2d(0, 0);
		glVertex3d(1600, -200, -7300);

		glTexCoord2d(0, 1);
		glVertex3d(1600, 250, -7300);

		glTexCoord2d(1, 1);
		glVertex3d(1600, 250, -7000);

		glTexCoord2d(1, 0);
		glVertex3d(1600, -200, -7000);
		glEnd();
		
	//fourth wall 
		glBindTexture(GL_TEXTURE_2D, wallMosque);
		glBegin(GL_QUADS);
		glTexCoord2d(0, 0);
		glVertex3d(1600, -200, -7000);

		glTexCoord2d(0, 1);
		glVertex3d(1600, 250, -7000);

		glTexCoord2d(1, 1);
		glVertex3d(1000, 250, -6700);

		glTexCoord2d(1, 0);
		glVertex3d(1000, -200, -6700);
		glEnd();
	
	
	
	// the ground
	//
		glPushMatrix();
		glBindTexture(GL_TEXTURE_2D, carpet);
		glBegin(GL_QUADS);
		glTexCoord2d(0, 0);
		glVertex3d(400, -200, -7600);

		glTexCoord2d(0, 9);
		glVertex3d(400, -200, -7600);

		glTexCoord2d(3, 6);
		glVertex3d(-200, -200, -7300);

		glTexCoord2d(3, 3);
		glVertex3d(-200, -200, -7000);
		glEnd();
		
		//
		glBindTexture(GL_TEXTURE_2D, carpet);
		glBegin(GL_QUADS);
		glTexCoord2d(0, 0);
		glVertex3d(1000, -200, -7600);

		glTexCoord2d(0, 9);
		glVertex3d(1000, -200, -6700);

		glTexCoord2d(3, 6);
		glVertex3d(1600, -200, -7000);

		glTexCoord2d(3, 3);
		glVertex3d(1600, -200, -7300);




		glEnd();


		//the middle rectangle 
		
		glBindTexture(GL_TEXTURE_2D, carpet);
		glBegin(GL_QUADS);
		glTexCoord2d(0, 0);
		glVertex3d(400, -200, -7600);

		glTexCoord2d(0, 9);
		glVertex3d(400, -200, -6700);

		glTexCoord2d(3, 9);
		glVertex3d(1000, -200, -6700);

		glTexCoord2d(3, 0); 
		glVertex3d(1000, -200, -7600);

		glEnd();
		glPopMatrix();
	}




	void Alaqsa::draw_the_top(int roof) 
	{
	
		glBindTexture(GL_TEXTURE_2D, roof);
		glBegin(GL_POLYGON);
		glTexCoord2f(0.0f, 0.0f);
		glVertex3d(400, 250, -6700);
		glTexCoord2f(1.0f, 0.0f);
		glVertex3d(-200, 250, -7000);
		glTexCoord2f(1.0f, 1.0f);
		glVertex3d(-200, 250, -7300);
		glTexCoord2f(0.0f, 1.0f);
		glVertex3d(400, 250, -7600);
		glTexCoord2f(0.5f, 1.0f);
		glVertex3d(1000, 250, -7600);
		glTexCoord2f(1.0f, 1.0f);
		glVertex3d(1600, 250, -7300);
		glTexCoord2f(1.0f, 0.0f);
		glVertex3d(1600, 250, -7000);
		glTexCoord2f(0.5f, 0.0f);
		glVertex3d(1000, 250, -6700);
		glEnd();

	 
}


void Alaqsa::DrawCylinder(float radius, float height, int num_segments,int texture = -1, Point temp = Point(0,0,0)) {
	glPushMatrix();
	if(texture != -1)
	{
		glBindTexture(GL_TEXTURE_2D, texture);
		//glTranslated(0.7, 4.7, -4.7);
		glTranslated(temp.x, temp.y, temp.z);
		glRotated(180, 0, 0, 1);

		glBegin(GL_QUADS);
		for (int i = 0; i < num_segments; i++) {
			float theta1 = 2.0f * 3.1415926f * float(i) / float(num_segments);
			float theta2 = 2.0f * 3.1415926f * float(i + 1) / float(num_segments);
			float s1 = float(i) / float(num_segments);
			float s2 = float(i + 1) / float(num_segments);
			// Side face
			glTexCoord2f(s1, 0);
			glVertex3f(radius * cosf(theta1), height, radius * sinf(theta1));
			glTexCoord2f(s2, 0);
			glVertex3f(radius * cosf(theta2), height, radius * sinf(theta2));
			glTexCoord2f(s2, 1);
			glVertex3f(radius * cosf(theta2), 0, radius * sinf(theta2));
			glTexCoord2f(s1, 1);
			glVertex3f(radius * cosf(theta1), 0, radius * sinf(theta1));
		}
		glEnd();
	}
	else {
		glBindTexture(GL_TEXTURE_2D, texture);
		//glTranslated(0.7, 4.7, -4.7);
		glTranslated(temp.x, temp.y, temp.z);
		glRotated(180, 0, 0, 1);

		glBegin(GL_QUADS);
		for (int i = 0; i < num_segments; i++) {
			float theta1 = 2.0f * 3.1415926f * float(i) / float(num_segments);
			float theta2 = 2.0f * 3.1415926f * float(i + 1) / float(num_segments);
			float s1 = float(i) / float(num_segments);
			float s2 = float(i + 1) / float(num_segments);
			// Side face
			glTexCoord2f(s1, 0);
			glVertex3f(radius * cosf(theta1), height, radius * sinf(theta1));
			glTexCoord2f(s2, 0);
			glVertex3f(radius * cosf(theta2), height, radius * sinf(theta2));
			glTexCoord2f(s2, 1);
			glVertex3f(radius * cosf(theta2), 0, radius * sinf(theta2));
			glTexCoord2f(s1, 1);
			glVertex3f(radius * cosf(theta1), 0, radius * sinf(theta1));
		}

		glEnd();
	}
	glPopMatrix();
	/////////////
	/*
	glPopMatrix();
	for (int i = 0; i <= 0; i++) {
		float theta1 = 2.0f * 3.1415926f * float(i) / float(num_segments);
		float theta2 = 2.0f * 3.1415926f * float(i + 1) / float(num_segments);

		glPushMatrix();
		glBegin(GL_QUADS);
		glVertex3d(-1, 2, 0);
		glVertex3d(2, 2, 0);
		glVertex3f(radius * cosf(theta1), height, radius * sinf(theta1));
		glVertex3f(radius * cosf(theta2), height, radius * sinf(theta2));
		glEnd();
		glPopMatrix();
	}
	*/
}



void Alaqsa::DrawDome(float rad, int texture , Point center)
{
	glPushMatrix();
	glTranslated(center.x, center.y, center.z);
	float lastcenter = rad * sin(3.14 / 2);
	float lastr = rad * cos(3.14 / 2);
	for (float vertical = 3.14 / 2.0; vertical >= 0; vertical -= 0.1)
	{


		float centeri = rad * sin(vertical);
		float ri = rad * cos(vertical);
		int col = 0;
		for (float horizin = 0; horizin <= 2 * 3.14 + 0.1; horizin += 0.1)
		{
			Point a = Point(ri * cos(horizin), centeri, ri * sin(horizin));
			Point b = Point(ri * cos(horizin + 0.1), centeri, ri * sin(horizin + 0.1));
			Point c = Point(lastr * cos(horizin), lastcenter, lastr * sin(horizin));
			Point d = Point(lastr * cos(horizin + 0.1), lastcenter, lastr * sin(horizin + 0.1));
			if (texture != -1)
			{
				glBindTexture(GL_TEXTURE_2D, texture);
				glBegin(GL_QUADS);
				glTexCoord2d(1 - (horizin) / (2 * 3.14), 0.5 + sin(vertical - 0.1) / 2.0);
				glVertex3f(a.x, a.y, a.z);
				glTexCoord2d(1 - (horizin + 0.1) / (2 * 3.14), 0.5 + sin(vertical - 0.1) / 2.0);
				glVertex3f(b.x, b.y, b.z);
				glTexCoord2d(1 - (horizin + 0.1) / (2 * 3.14), 0.5 + sin(vertical) / 2.0);
				glVertex3f(d.x, d.y, d.z);
				glTexCoord2d(1 - (horizin) / (2 * 3.14), 0.5 + sin(vertical) / 2.0);
				glVertex3f(c.x, c.y, c.z);
				glEnd();
			}
			else
			{
				glBegin(GL_QUADS);
				glVertex3f(a.x, a.y, a.z);
				glVertex3f(b.x, b.y, b.z);
				glVertex3f(d.x, d.y, d.z);
				glVertex3f(c.x, c.y, c.z);
				glEnd();
			}
		}
		lastr = ri;
		lastcenter = centeri;
	}
	//glTranslated(-center.getX(), -center.getY(), -center.getZ());

	glPopMatrix();
}