#pragma once
#include "Point.h"
#include "Skybox.h"

class Alqibli
{
public:
	static void Mosque(int back, int left, int right, int front,int top, 
		               Point start, float width, float length, float depth);
	static void Door(int door, Point start, float width, float length);
	static void pyramid(int image, Point one, Point two, Point top1, Point three, Point four, Point top2);
	static void Dome(float radius);
	static void Pillar(int image, Point start, float width, float length, float depth);
	static void Column(Point start, float rad, float depth);
};

