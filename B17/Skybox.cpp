#include "SkyBox.h"
#include <windows.h>
#include <gl\gl.h>
#include <ctime>
#include <cmath>
#include "texture.h"
#include "Model_3DS.h"


void SkyBox::Sky(int back , int left , int right , int front , int top , int bottom , Point start ,
                   float width , float length , float depth , int wraps , int wrapt)
{
//back
glColor3ub(255,255,255);
glBindTexture(GL_TEXTURE_2D, back);
glBegin(GL_QUADS);
glTexCoord2d(0,0);
glVertex3f(start.x, start.y, start.z);
glTexCoord2d(1,0);
glVertex3f(start.x + width, start.y, start.z);
glTexCoord2d(1,1);
glVertex3f(start.x + width, start.y + length, start.z);
glTexCoord2d(0,1);
glVertex3f(start.x, start.y + length, start.z);
glEnd();

//left
glColor3ub(255,255,255);
glBindTexture(GL_TEXTURE_2D, left);
glBegin(GL_QUADS);
glTexCoord2d(0,0);
glVertex3f(start.x + width, start.y, start.z);
glTexCoord2d(wraps,0);
glVertex3f(start.x + width, start.y, start.z + depth);
glTexCoord2d(wraps,wrapt);
glVertex3f(start.x + width, start.y + length, start.z + depth);
glTexCoord2d(0,wrapt);
glVertex3f(start.x + width, start.y + length, start.z);
glEnd();

//right
glColor3ub(255,255,255);
glBindTexture(GL_TEXTURE_2D, right);
glBegin(GL_QUADS);
glTexCoord2d(0,0);
glVertex3f(start.x, start.y, start.z + depth);
glTexCoord2d(wraps,0);
glVertex3f(start.x, start.y, start.z);
glTexCoord2d(wraps,wrapt);
glVertex3f(start.x, start.y + length, start.z);
glTexCoord2d(0,wrapt);
glVertex3f(start.x, start.y + length, start.z + depth);
glEnd();

//front
glColor3ub(255,255,255);
glBindTexture(GL_TEXTURE_2D, front);
glBegin(GL_QUADS);
glTexCoord2d(0,0);
glVertex3f(start.x + width, start.y, start.z + depth);
glTexCoord2d(1,0);
glVertex3f(start.x, start.y, start.z + depth);
glTexCoord2d(1,1);
glVertex3f(start.x, start.y + length, start.z + depth);
glTexCoord2d(0,1);
glVertex3f(start.x + width, start.y + length, start.z + depth);
glEnd();

//top
glColor3ub(255,255,255);
glBindTexture(GL_TEXTURE_2D, top);
glBegin(GL_QUADS);
glTexCoord2d(0,0);
glVertex3f(start.x, start.y + length, start.z + depth);
glTexCoord2d(wraps,0);
glVertex3f(start.x, start.y + length, start.z);
glTexCoord2d(wraps,wrapt);
glVertex3f(start.x + width, start.y + length, start.z);
glTexCoord2d(0,1);
glVertex3f(start.x + width, start.y + length, start.z + depth);
glEnd();

//bottom
glColor3ub(255,255,255);
glBindTexture(GL_TEXTURE_2D, bottom);
glBegin(GL_QUADS);
glTexCoord2d(0,0);
glVertex3f(start.x, start.y, start.z);
glTexCoord2d(10,0);
glVertex3f(start.x + width, start.y, start.z);
glTexCoord2d(10,15);
glVertex3f(start.x + width, start.y , start.z + depth);
glTexCoord2d(0,15);
glVertex3f(start.x, start.y , start.z + depth);
glEnd();
}