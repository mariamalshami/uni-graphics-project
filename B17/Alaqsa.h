#pragma once
#include "Point.h"

class Alaqsa
{
public:
	static void Draw_alaqsa(int door, int wallMosque, int carpet);
	static void draw_the_top(int roof);
	static void DrawCylinder(float radius, float height, int num_segments, int texture, Point temp);
	static void DrawDome(float rad, int texture, Point center);
};

 
