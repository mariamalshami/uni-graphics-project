#include "Alqibli.h"
#include <windows.h>
#include <gl\gl.h>
#include <ctime>
#include <cmath>
#include "texture.h"
#include "Model_3DS.h"

void Alqibli::Mosque(int back, int left, int right, int front, int top, 
				Point start, float width , float length, float depth)
{
	glColor3ub(255,255,255);
	//back
glColor3ub(255,255,255);
glBindTexture(GL_TEXTURE_2D, back);
glBegin(GL_QUADS);
glTexCoord2d(0,0);
glVertex3f(start.x, start.y, start.z);
glTexCoord2d(1,0);
glVertex3f(start.x + width, start.y, start.z);
glTexCoord2d(1,1);
glVertex3f(start.x + width, start.y + length, start.z);
glTexCoord2d(0,1);
glVertex3f(start.x, start.y + length, start.z);
glEnd();

//left
glColor3ub(255,255,255);
glBindTexture(GL_TEXTURE_2D, left);
glBegin(GL_QUADS);
glTexCoord2d(0,0);
glVertex3f(start.x + width, start.y, start.z);
glTexCoord2d(1,0);
glVertex3f(start.x + width, start.y, start.z + depth);
glTexCoord2d(1,1);
glVertex3f(start.x + width, start.y + length, start.z + depth);
glTexCoord2d(0,1);
glVertex3f(start.x + width, start.y + length, start.z);
glEnd();

//right
glColor3ub(255,255,255);
glBindTexture(GL_TEXTURE_2D, right);
glBegin(GL_QUADS);
glTexCoord2d(0,0);
glVertex3f(start.x, start.y, start.z + depth);
glTexCoord2d(1,0);
glVertex3f(start.x, start.y, start.z);
glTexCoord2d(1,1);
glVertex3f(start.x, start.y + length, start.z);
glTexCoord2d(0,1);
glVertex3f(start.x, start.y + length, start.z + depth);
glEnd();

//front
glColor3ub(255,255,255);
glBindTexture(GL_TEXTURE_2D, front);
glBegin(GL_QUADS);
glTexCoord2d(0,0);
glVertex3f(start.x + width, start.y, start.z + depth);
glTexCoord2d(1,0);
glVertex3f(start.x, start.y, start.z + depth);
glTexCoord2d(1,1);
glVertex3f(start.x, start.y + length, start.z + depth);
glTexCoord2d(0,1);
glVertex3f(start.x + width, start.y + length, start.z + depth);
glEnd();

//top
glColor3ub(255,255,255);
glBindTexture(GL_TEXTURE_2D, top);
glBegin(GL_QUADS);
glTexCoord2d(0,0);
glVertex3f(start.x, start.y + length, start.z + depth);
glTexCoord2d(1,0);
glVertex3f(start.x, start.y + length, start.z);
glTexCoord2d(1,1);
glVertex3f(start.x + width, start.y + length, start.z);
glTexCoord2d(0,1);
glVertex3f(start.x + width, start.y + length, start.z + depth);
glEnd();
}

void Alqibli::Door(int door, Point start, float width, float length)
{
	//door
glBindTexture(GL_TEXTURE_2D, door);
glBegin(GL_QUADS);
glTexCoord2d(0,0);
glVertex3f(start.x, start.y, start.z);
glTexCoord2d(1,0);
glVertex3f(start.x + width, start.y, start.z);
glTexCoord2d(1,1);
glVertex3f(start.x + width, start.y + length, start.z);
glTexCoord2d(0,1);
glVertex3f(start.x, start.y + length, start.z);
glEnd();
}

void Alqibli::pyramid(int image, Point one, Point two, Point top1, Point three, Point four, Point top2)
{
	glColor3ub(255,255,255);
	glBindTexture(GL_TEXTURE_2D, image);
	glBegin(GL_TRIANGLES);
	glTexCoord2d(0,0);
    glVertex3f(one.x, one.y, one.z);
	glTexCoord2d(1,0);
	glVertex3f(two.x, two.y, two.z); 
	glTexCoord2d(1,1);
    glVertex3f(top1.x, top1.y, top1.z); 

	glTexCoord2d(0,0);
    glVertex3f(three.x, three.y, three.z);
	glTexCoord2d(1,0);
    glVertex3f(four.x, four.y, four.z); 
	glTexCoord2d(1,1);
    glVertex3f(top2.x, top2.y, top2.z); 

    glEnd();

    glBegin(GL_QUADS);
	glTexCoord2d(0,0);
    glVertex3f(one.x, one.y, one.z); 
	glTexCoord2d(1,0);
    glVertex3f(top1.x, top1.y, top1.z); 
	glTexCoord2d(1,1);
    glVertex3f(top2.x, top2.y, top2.z); 
	glTexCoord2d(0,1);
    glVertex3f(three.x, three.y, three.z); 


	glTexCoord2d(0,0);
    glVertex3f(two.x, two.y, two.z); 
	glTexCoord2d(1,0);
    glVertex3f(top1.x, top1.y, top1.z); 
	glTexCoord2d(1,1);
    glVertex3f(top2.x, top2.y, top2.z); 
	glTexCoord2d(0,1);
    glVertex3f(four.x, four.y, four.z); 

    glEnd();
	
}


double angle, angle2, i, j, a, a2,
	  x, y, z, 
	  x2, y2, z2,
	  x3, y3, z3,
	  x4, y4, z4;
double PI=3.14;

void Alqibli::Dome(float radius)
{
		//glEnable(GL_TEXTURE_2D);
		//glBindTexture(GL_TEXTURE_2D, imab);    
	 	 glBegin(GL_LINES); 
    for (i = 0; i < 100; i++) {
		angle = 2.0 * PI *i / 200; 
        angle2 = 2.0 * PI * (i + 1) / 200; 

        for (j = 0; j < 200; j++) {
			a = PI * j / 200;
			a2 = PI * (j + 1) / 200; 

			x = radius*sin(a) * cos(angle);
			y = radius*sin(a) * sin(angle);
            z = radius*cos(a);
            
			x2 = radius*sin(a) * cos(angle2);
			y2 = radius*sin(a) * sin(angle2);
            z2 = radius*cos(a);
            
			x3 = radius*sin(a2) * cos(angle);
			y3 = radius*sin(a2) * sin(angle);
            z3 = radius*cos(a2);
            
		    x4 = radius*sin(a2) * cos(angle2);
            y4 = radius*sin(a2) * sin(angle2);
            z4 = radius*cos(a2);

            glVertex3d(x, y, z);
            glVertex3d(x2, y2, z2);

            glVertex3d(x, y, z);
            glVertex3d(x3, y3, z3);

            glVertex3d(x3, y3, z3);
            glVertex3d(x4, y4, z4);

            glVertex3d(x2, y2, z2);
            glVertex3d(x4, y4, z4);
        }
    }
    glEnd();
}

void Alqibli::Pillar(int image, Point start, float width, float length, float depth)
{
	//one
	SkyBox::Sky(image, image, image, image, image, image, Point (start.x - 40, start.y, start.z), 
		        width, length, depth, 1, 1);
	//two
	SkyBox::Sky(image, image, image, image, image, image, Point (start.x + 200, start.y, start.z), 
		        width, length, depth, 1, 1);
	//three
	SkyBox::Sky(image, image, image, image, image, image, Point (start.x + 440, start.y, start.z), 
		        width, length, depth, 1, 1);
	//four
	SkyBox::Sky(image, image, image, image, image, image, Point (start.x + 680, start.y, start.z), 
		        width, length, depth, 1, 1);
	//five
	SkyBox::Sky(image, image, image, image, image, image, Point (start.x + 920, start.y, start.z), 
		        width, length, depth, 1, 1);
	//six
	SkyBox::Sky(image, image, image, image, image, image, Point (start.x + 1160, start.y, start.z), 
		        width, length, depth, 1, 1);


	//top
	glBindTexture(GL_TEXTURE_2D, image);
	glBegin(GL_QUADS);
	glTexCoord2d(0,0);
	glVertex3d(start.x - 40, start.y + length + 150, start.z);
	glTexCoord2d(1,0);
	glVertex3d(start.x + 1200, start.y + length + 150, start.z);
	glTexCoord2d(1,1);
	glVertex3d(start.x + 1200, start.y + length + 150, start.z + depth);
	glTexCoord2d(0,1);
	glVertex3d(start.x - 40, start.y + length + 150, start.z + depth);
	glEnd();

	//right
	glBindTexture(GL_TEXTURE_2D, image);
	glBegin(GL_QUADS);
	glTexCoord2d(0,0);
	glVertex3d(start.x + 1200, start.y, start.z);
	glTexCoord2d(1,0);
	glVertex3d(start.x + 1200, start.y + length + 150, start.z);
	glTexCoord2d(1,1);
	glVertex3d(start.x + 1140, start.y + length + 150, start.z + depth);
	glTexCoord2d(0,1);
	glVertex3d(start.x + 1140, start.y, start.z + depth);
	glEnd();

	//left
	glBindTexture(GL_TEXTURE_2D, image);
	glBegin(GL_QUADS);
	glTexCoord2d(0,0);
	glVertex3d(start.x - 40, start.y, start.z);
	glTexCoord2d(1,0);
	glVertex3d(start.x - 40, start.y + length + 150, start.z);
	glTexCoord2d(1,1);
	glVertex3d(start.x - 40, start.y + length + 150, start.z + depth);
	glTexCoord2d(0,1);
	glVertex3d(start.x - 40, start.y, start.z + depth);
	glEnd();

	//back
	//left
	glBindTexture(GL_TEXTURE_2D, image);
	glBegin(GL_QUADS);
	glTexCoord2d(0,0);
	glVertex3d(start.x + 1200, start.y, start.z + depth);
	glTexCoord2d(1,0);
	glVertex3d(start.x + 1200, start.y + length + 150, start.z + depth);
	glTexCoord2d(1,1);
	glVertex3d(start.x + 1100, start.y + length + 150, start.z + depth);
	glTexCoord2d(0,1);
	glVertex3d(start.x + 1100, start.y, start.z + depth);
	glEnd();
	//right
	glBindTexture(GL_TEXTURE_2D, image);
	glBegin(GL_QUADS);
	glTexCoord2d(0,0);
	glVertex3d(start.x - 40, start.y, start.z + depth);
	glTexCoord2d(1,0);
	glVertex3d(start.x - 40, start.y + length + 150, start.z + depth);
	glTexCoord2d(1,1);
	glVertex3d(start.x, start.y + length + 150, start.z + depth);
	glTexCoord2d(0,1);
	glVertex3d(start.x, start.y, start.z + depth);
	glEnd();
}

void Alqibli::Column(Point start, float rad, float depth)
{
	glPushMatrix();
	glTranslated(start.x, start.y, 0);
	glRotated(90, 0, 0, 1);
	glBegin(GL_LINES);
	for(double i=0; i<=3.14; i+=0.0005)
	{
		 double x = rad * sin(i);
		double y = rad * cos(i);
		glVertex3d(x, y, start.z);
		glVertex3d(x, y, start.z - depth);
    }
	glEnd();
	glPopMatrix();

	glPushMatrix();
	glTranslated(start.x + 240, start.y, 0);
	glRotated(90, 0, 0, 1);
	glBegin(GL_LINES);
	for(double i=0; i<=3.14; i+=0.0005)
	{
		 double x = rad * sin(i);
		double y = rad * cos(i);
		glVertex3d(x, y, start.z);
		glVertex3d(x, y, start.z - depth);
	}
	glEnd();
	glPopMatrix();

	glPushMatrix();
	glTranslated(start.x + 480, start.y, 0);
	glRotated(90, 0, 0, 1);
	glBegin(GL_LINES);
	for(double i=0; i<=3.14; i+=0.0005)
	{
		 double x = rad * sin(i);
		double y = rad * cos(i);
		glVertex3d(x, y, start.z);
		glVertex3d(x, y, start.z - depth);
	}
	glEnd();
	glPopMatrix();

	glPushMatrix();
	glTranslated(start.x + 720, start.y, 0);
	glRotated(90, 0, 0, 1);
	glBegin(GL_LINES);
	for(double i=0; i<=3.14; i+=0.0005)
	{
		 double x = rad * sin(i);
		double y = rad * cos(i);
		glVertex3d(x, y, start.z);
		glVertex3d(x, y, start.z - depth);
	}
	glEnd();
	glPopMatrix();

	glPushMatrix();
	glTranslated(start.x + 960, start.y, 0);
	glRotated(90, 0, 0, 1);
	glBegin(GL_LINES);
	for(double i=0; i<=3.14; i+=0.0005)
	{
		 double x = rad * sin(i);
		double y = rad * cos(i);
		glVertex3d(x, y, start.z);
		glVertex3d(x, y, start.z - depth);
	}
	glEnd();
	glPopMatrix();

}